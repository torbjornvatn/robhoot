## Robohoot
- Api-token?
- sbt-basert-klient
- Event sourcing / audit log

## Robot part inventory oppgave
The Kahoot R&D department wants to see if Kahooters can order and assemble robots that they can control using 
our platform. The first step is to set up a catalogue of the robot parts and a compatibility mapping. 
To get started, we’ll set up a web service for the inventory of the robot parts.

Each robot part has a:
 
- name
- serial number 
- manufacturer 
- weight 
- a list of other components it is compatible with within the system. 

The service has the following endpoints:

- add
- read
- update
- delete
- list all parts
- list X unique robot parts that are compatible

We also need a very simple client that can hook up to the web service.

### What to do
- Give a simple sketch of the solution
- Outline database model
- Implement the web service
- Implement the Client
- Identify possible scalability and high availability problems in your solution and explain how you would detect and mitigate them
- How would you secure access to it, deploy it etc?

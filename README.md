               _____
              |     |
              | | | |
              |_____|
        ____ ___|_|___ ____
       ()___)         ()___)
       // /|           |\ \\
      // / |           | \ \\
     (___) |___________| (___)
     (___)   (_______)   (___)
     (___)     (___)     (___)
     (___)      |_|      (___)
     (___)  ___/___\___   | |
      | |  |           |  | |
      | |  |___________| /___\
     /___\  |||     ||| //   \\
    //   \\ |||     ||| \\   //
    \\   // |||     |||  \\ //
     \\ // ()__)   (__()
           ///       \\\
          ///         \\\
        _///___     ___\\\_
       |_______|   |_______|

# Robhoot

## The solution
### Web service
I've chosen to implement the web service in **Scala** using [Unfiltered](http://unfiltered.databinder.net/Unfiltered.html) as my HTTP library and [Slick](http://slick.typesafe.com/) 
to access the database.

To start the web service server run this commands at the root of the project (requires sbt, which can be installed via homebrew or from [scala-sbt.org](http://www.scala-sbt.org/))
    
    
    $> sbt stage
    $> target/universal/stage/bin/robhoot-server
   
The service has the following endpoints: 

    // Get all parts
    GET /robotParts
    
    // Get a specific part
    GET /robotParts/1
    
    // Delete a part
    DELETE /robotParts/2
    
    // Add a new part
    POST /robotParts
    {
      "part": {
        "name": "cigar",
        "serialNumber": "SN/123",
        "manufacturer": "Bolivar",
        "weight": 0.08
      },
      "compatibleParts": [1]
    }
    
    // Update a specific part
    PUT /robotParts/5
    {
      "part": {
        "name": "cigar",
        "serialNumber": "SN/99999",
        "manufacturer": "Le Grand Cigar",
        "weight": 0.09
      },
      "compatibleParts": [1,2]
    }
    
    // Print X unique compatible parts
    GET /compatibleParts?numberOfParts=1
    
    // Prints the database tables
    GET /robotParts/schema

### Cli client 
The test client is a cli that calls the web service using [Dispatch](http://dispatch.databinder.net/Dispatch.html) leveraging SBT's cli parser to get tab completion  

To fire up the client run this commands at the root of the project:
    
    $> sbt cli/stage
    $> cli/target/universal/stage/bin/robhoot-cli
    
## The database model
    
There's not much of a model actually.

![Database schema](database.png)

It's just a simple table for the robot parts with a numeric, auto incremented id as the primary key. 
The compatability between parts is tracked in a separate table with the related part ids as a compound primary key.

## Possible scalability and high availability problems

The web service it self could be deployed on several servers behind a loadbalancer to handle the load. 
In a CRUD based app like this the most likely perfomance issues would be related to the single database, so some kind of syncing with a scaleable search engine like Elasticsearch could be a good idea.
 
At my current job we have looked into Datadog for monitoring of servers and databases, and it looks promising. It uses agents on the servers to monitor CPU and memory load, JVM stats and database metrics.
It also support metrics reporting with [Metrics](https://dropwizard.github.io/metrics/3.1.0/) from Dropwizard which allows us to add timers and gauges within our code to track the performance of the different
parts of the app. It can off course be configured to alert us when some of the metrics exceeds a threshold, and the events and metrics can be view in a combined timeline to see of they relate to eachother.  

## Secure access and deployment

First of all I'd use Https in production to protect the privacy and integrity of the exchanged data.

To authenticate the users I'd probably use a OAuth based login mechanism and apps like the Cli client would need to add a Access token header. The robots themselves could also use this token to connect to the API.   
The tokens would have to be generated in some kind of account management page behind the OAuth login.

When it comes to deployment I'd use a cloud provider that can enable dynamic scaling of the web service servers, for example Amazon or Digital Ocean. 
    

object Run extends App {

  println(Logo.robot.stripMargin)
  println("\n")
  println("I'm running on http://localhost:5007")

  val endpoints = new Endpoints {
    val repo = new H2BackedRobotPartsRepo
  }

  unfiltered.jetty.Server.local(5007).plan(endpoints.robotParts).run()

}

object Logo {
  val robot = """
|                                  _____
|                                 |     |
|                                 | | | |
|                                 |_____|
|                           ____ ___|_|___ ____
|                          ()___)         ()___)
|                          // /|           |\ \\
|                         // / |           | \ \\
|                        (___) |___________| (___)
|                        (___)   (_______)   (___)
|                        (___)     (___)     (___)
|                        (___)      |_|      (___)
|                        (___)  ___/___\___   | |
|                         | |  |           |  | |
|                         | |  |___________| /___\
|                        /___\  |||     ||| //   \\
|                       //   \\ |||     ||| \\   //
|                       \\   // |||     |||  \\ //
|                        \\ // ()__)   (__()
|                              ///       \\\
|                             ///         \\\
|                           _///___     ___\\\_
|                          |_______|   |_______|
|
"""
}

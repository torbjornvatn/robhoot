import argonaut._, Argonaut._

object JsonCodecs {

  def compatiblePartToObject(cp: RobotPart) =
    ("partId" := cp.partId.value) ->:
    ("name"   := cp.name.value)   ->:
    jEmptyObject

  implicit val PartIdEncoder      = CodecJson.derive[PartId]
  implicit val PartNameCodec      = CodecJson.derive[PartName]
  implicit val SerialNumberCodec  = CodecJson.derive[SerialNumber]
  implicit val ManufacturerCodec  = CodecJson.derive[Manufacturer]
  implicit val WeightCodec        = CodecJson.derive[Weight]
  implicit val RobotPartCodec: CodecJson[RobotPart] =
    CodecJson[RobotPart](
      encoder = rp ⇒ {
        Json.obj(
          "partId"           := rp.partId.value,
          "name"             := rp.name.value,
          "serialNumber"     := rp.serialNumber.value,
          "manufacturer"     := rp.manufacturer.value,
          "weight"           := rp.weight.kg
        )
      },
      decoder = c =>
        for {
          name          ← (c --\ "name").>->(j ⇒ ("value" := j)         ->: jEmptyObject).as[PartName]
          serialNumber  ← (c --\ "serialNumber").>->(j ⇒ ("value" := j) ->: jEmptyObject).as[SerialNumber]
          manufacturer  ← (c --\ "manufacturer").>->(j ⇒ ("value" := j) ->: jEmptyObject).as[Manufacturer]
          weight        ← (c --\ "weight").>->(j ⇒ ("kg" := j)          ->: jEmptyObject).as[Weight]
        } yield RobotPart(PartId(Int.MaxValue), name, serialNumber, manufacturer, weight)
    )
  implicit val RobotPartWCompatibilitiesEncoder: EncodeJson[RobotPartWithCompatibilities] =
    EncodeJson[RobotPartWithCompatibilities](
      rpwc ⇒
        Json.obj(
          "part"            := rpwc.part,
          "compatibleParts" := rpwc.compatibleParts map (p ⇒ Json.obj("name" := p.name.value, "partId" := p.partId.value))
        )
    )
  implicit val RobotPartWCompatibleIdsDecoder: DecodeJson[RobotPartWithCompatibleIds] =
    DecodeJson[RobotPartWithCompatibleIds](
      c ⇒ for {
        part            ← (c --\ "part").as[RobotPart]
        compatibleParts ← (c --\ "compatibleParts").as[List[Int]]
      } yield RobotPartWithCompatibleIds(part, compatibleParts.map(PartId))
    )

}

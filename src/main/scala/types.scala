import java.util.UUID

case class PartId(value: Int)                           extends AnyVal {
  def inc = this.copy(value = value + 1)
}
case class PartName(value: String)                      extends AnyVal
case class SerialNumber(value: String)                  extends AnyVal
object SerialNumber {
  def random = SerialNumber(UUID.randomUUID().toString)
}
case class Manufacturer(value: String)                  extends AnyVal
object Manufacturer {
  val Hitachi         = Manufacturer("Hitachi")
  val Honda           = Manufacturer("Honda")
  val BostonDynamics  = Manufacturer("Boston Dynamics")
}
case class Weight(kg: Double)                           extends AnyVal

case class RobotPart(partId: PartId,
                     name: PartName,
                     serialNumber: SerialNumber,
                     manufacturer: Manufacturer,
                     weight: Weight)

case class RobotPartWithCompatibilities(part: RobotPart, compatibleParts: List[RobotPart])
case class RobotPartWithCompatibleIds(part: RobotPart, compatiblePartIds: List[PartId])

object typesImplicits {
  implicit class Double2Wight(d: Double) {
    def kg = Weight(d)
  }
}

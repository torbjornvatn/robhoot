import unfiltered.directives._, Directives._
import unfiltered.request._
import unfiltered.response._
import argonaut._, Argonaut._
import JsonCodecs._

import scala.util.{Failure, Success, Try}
import scalaz.{-\/, \/-, \/}

trait Endpoints extends DirectiveUtils {

  def repo: RobotPartsRepo

  val robotParts = unfiltered.filter.Planify { Directive.Intent.Path {

    case "/robotParts" ⇒

      val getAllParts = for {
        _ ← GET
      } yield JsonContent ~>
        ResponseString(repo.getAllRobotParts.asJson.spaces4)

      val addNewPart = for {
        _         ← POST
        r         ← request[Any]
        body      = Body string r
        robotPart ← Parse.decodeEither[RobotPartWithCompatibleIds](body).
          disjunctionToDirective(err ⇒ BadRequest ~> ResponseString(s"Invalid robot part json: $body \n $err"))
      } yield Created ~> JsonContent ~>
          ResponseString(s"New robot part added with ${repo.insertOrUpdateRobotPart(robotPart)}")

      getAllParts | addNewPart

    case Seg("robotParts" :: "schema" :: Nil) ⇒
      for {
        _ ← GET
      } yield ResponseString(repo.printDatabaseSchema)

    case Seg("robotParts" :: partId :: Nil) ⇒

      val getSpecificPart = for {
        _   ← GET
        pi ← Try(PartId(partId.toInt)).tryToDirective
      } yield repo.getRobotPart(pi) match {
          case Some(rp) ⇒ JsonContent ~> ResponseString(rp.asJson.spaces4)
          case None     ⇒ NotFound    ~> ResponseString(s"Can't find a robot part with $pi")
        }

      val updatePart = for {
        _         ← PUT
        r         ← request[Any]
        body      = Body string r
        pi        ← Try(PartId(partId.toInt)).tryToDirective
        robotPart ← Parse.decodeEither[RobotPartWithCompatibleIds](body).
          disjunctionToDirective(err ⇒ BadRequest ~> ResponseString(s"Invalid robot part json: $body \n $err"))
      } yield Created ~> JsonContent ~>
          ResponseString(s"Robot part with ${
            repo.insertOrUpdateRobotPart(robotPart.copy(part = robotPart.part.copy(partId = pi))) match {
              case Some(pId) ⇒ s"$pId created"
              case None ⇒ s"$pi updated"
            }
          }")

      val deleteSpecificPart = for {
        _   ← DELETE
        pi  ← Try(PartId(partId.toInt)).tryToDirective
      } yield {
        val rows = repo.deleteRobotPart(pi)
        JsonContent ~> ResponseString(s"Deleted $pi, $rows rows affected")
      }
      getSpecificPart | deleteSpecificPart | updatePart

    case Seg("compatibleParts" :: Nil) ⇒
      for {
        _             ← GET
        numberOfParts ← data.as.Int named "numberOfParts"
      } yield numberOfParts match {
        case Some(nr) ⇒ JsonContent ~> ResponseString(repo.getXCompatibleParts(nr).asJson.spaces4)
        case None ⇒ BadRequest ~> ResponseString("numberOfParts query parameter is required!")
      }

    case _ ⇒
      for {
        _ ← GET | POST
      } yield NotFound ~> ResponseString("WAT!?!")

  }}
  
}

trait DirectiveUtils {

  type SimpleDirective[A] = Directive[Any, ResponseFunction[Any], A]

  implicit class TryToDirective[A](er: => Try[A]) {
    def tryToDirective: SimpleDirective[A] = {
      Directive[Any, ResponseFunction[Any], A] {
        _ =>
          er match {
            case Success(s) => Result.Success(s)
            case Failure(l)  => Result.Failure(BadRequest ~> ResponseString(l.getMessage))
          }
      }
    }
  }

  implicit class DisjunctionToDirective[A, B](disjunction: => A \/ B) {
    def disjunctionToDirective(rf: A ⇒ ResponseFunction[Any]): SimpleDirective[B] = {
      Directive[Any, ResponseFunction[Any], B] {
        _ =>
          disjunction match {
            case \/-(r) => Result.Success(r)
            case -\/(l)  => Result.Failure(rf(l))
          }
      }
    }
  }

}
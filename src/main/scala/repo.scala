import scala.language.postfixOps

trait RobotPartsRepo {
  def insertOrUpdateRobotPart(part: RobotPartWithCompatibleIds): Option[PartId]
  def getRobotPart(partId: PartId): Option[RobotPartWithCompatibilities]
  def getAllRobotParts: List[RobotPartWithCompatibilities]
  def deleteRobotPart(partId: PartId): Int
  def getXCompatibleParts(numberOfParts: Int): Set[RobotPart]
  def printDatabaseSchema: String
}

class H2BackedRobotPartsRepo extends RobotPartsRepo with Tables with Setup {

  import slick.driver.H2Driver.api._

  import scala.concurrent.Await
  import scala.concurrent.duration._

  def await[R](a: DBIOAction[R, NoStream, Nothing]): R = Await.result(db.run(a), 1 second)

  val db = Database.forConfig("h2mem1")
  Await.result(db.run(setupDb), 1 second)
  Await.result(db.run(insertCompatibleRobotParts()), 1 second)

  override def insertOrUpdateRobotPart(partWithCompatabilities: RobotPartWithCompatibleIds): Option[PartId] = {
    val RobotPartWithCompatibleIds(part, compatiblePartIds) = partWithCompatabilities

    val id = await { (robotParts returning robotParts.map(_.partId)) insertOrUpdate part }
    println(s"Part with ${
      id match {
        case Some(i)  ⇒ s"$i inserted"
        case None     ⇒ s"${part.partId} updated"
      }
    } ")
    val compatabilities = compatiblePartIds map (cpId ⇒ id.getOrElse(part.partId) → cpId)

    await {DBIO.sequence(compatabilities map {c ⇒ robotPartCompatabilities.insertOrUpdate(c)})}
    println(s"Comapatbilities $compatabilities updated")

    id
  }

  override def deleteRobotPart(partId: PartId): Int = await {
    robotParts.filter(_.partId === partId).delete               andThen
    robotPartCompatabilities.filter(_.partId === partId).delete andThen
    robotPartCompatabilities.filter(_.comaptiblePartId === partId).delete
  }

  override def getRobotPart(partId: PartId): Option[RobotPartWithCompatibilities] = for {
    part ← await(robotParts.filter(_.partId === partId).result).headOption
  } yield RobotPartWithCompatibilities(part, getCompatibleParts(partId))

  override def getAllRobotParts: List[RobotPartWithCompatibilities] = {
    await(robotParts.result).toList map { part ⇒
      RobotPartWithCompatibilities(part, getCompatibleParts(part.partId))
    }
  }

  private def getCompatibleParts(partId: PartId): List[RobotPart] = await {
    (for {
      compatiblePartId  ← robotPartCompatabilities.filter(_.partId === partId).map(_.comaptiblePartId)
      compatiblePart    ← robotParts.filter(_.partId === compatiblePartId)
    } yield compatiblePart).result
  }.toList

  override def getXCompatibleParts(numberOfParts: Int): Set[RobotPart] = {
    val parts = await {
      val compatiblePartsQuery = numberOfParts match {
        case 0 ⇒ robotPartCompatabilities
        case n ⇒ robotPartCompatabilities.take(n)
      }

      (for {
        compatiblePart  ← compatiblePartsQuery
        part            ← robotParts.filter(p ⇒ p.partId === compatiblePart.partId || p.partId === compatiblePart.comaptiblePartId)
      } yield part).result
    }.toSet
    if (numberOfParts > 0)
      parts.take(numberOfParts)
    else
      parts
  }

  override def printDatabaseSchema: String = {
    val robotPartsSchema = robotParts.schema.createStatements.mkString("\n")

    val robotPartCompatabilitiesSchema = robotPartCompatabilities.schema.createStatements.mkString("\n")

    s"""
       |---------------------------------------
       |robot_parts
       |---------------------------------------
       |$robotPartsSchema
       |
       |---------------------------------------
       |robot_part_comapatbility
       |---------------------------------------
       |$robotPartCompatabilitiesSchema
     """.stripMargin
  }
}

trait Tables {

  import slick.driver.H2Driver.api._
  import MappedColumnType.base

  implicit lazy val partIdMapper = base[PartId, Int](pi ⇒ pi.value, PartId)
  implicit lazy val partNameMapper = base[PartName, String](pn ⇒ pn.value, PartName)
  implicit lazy val serialNrMapper = base[SerialNumber, String](sn ⇒ sn.value, SerialNumber(_))
  implicit lazy val manufacturerMapper = base[Manufacturer, String](m ⇒ m.value, Manufacturer(_))
  implicit lazy val weightMapper = base[Weight, Double](w ⇒ w.kg, Weight)

  class RobotPartT(tag: Tag) extends Table[(RobotPart)](tag, "robot_parts") {
    def partId = column[PartId]("part_id", O.PrimaryKey, O.AutoInc)
    def name = column[PartName]("name")
    def serialNumber = column[SerialNumber]("serial_number")
    def manufacturer = column[Manufacturer]("manufacturer")
    def weight = column[Weight]("weight")

    override def * = (partId, name, serialNumber, manufacturer, weight) <>(RobotPart.tupled, RobotPart.unapply)
  }

  val robotParts = TableQuery[RobotPartT]

  class RobotPartCompatability(tag: Tag) extends Table[(PartId, PartId)](tag, "robot_part_comapatbility") {
    def partId = column[PartId]("part_id")
    def comaptiblePartId = column[PartId]("comaptible_part_id")

    val pk = primaryKey("robot_part_compatability_pk", partId → comaptiblePartId)

    override def * = (partId, comaptiblePartId)
  }

  val robotPartCompatabilities = TableQuery[RobotPartCompatability]
}

trait Setup {
  self: RobotPartsRepo with Tables =>

  import slick.driver.H2Driver.api._
  import typesImplicits._

  def setupDb = DBIO.seq(
    robotParts.schema.create,
    robotPartCompatabilities.schema.create,
    robotParts ++= initialRobotParts
  )

  def insertCompatibleRobotParts() = {
    val allParts = (getAllRobotParts map { pwc ⇒
      val part  = pwc.part
      part.name.value → part
    }).toMap

    robotPartCompatabilities ++= allParts.values.toSeq flatMap {
      case RobotPart(id, PartName("Body"), _, _, _) ⇒
        List(
          id → allParts("Arm").partId,
          id → allParts("Leg").partId,
          id → allParts("Head").partId
        )
      case RobotPart(id, PartName("Arm"), _, _, _) ⇒ (id → allParts("Body").partId) :: Nil
      case RobotPart(id, PartName("Leg"), _, _, _) ⇒ (id → allParts("Body").partId) :: Nil
      case RobotPart(id, PartName("Head"), _, _, _) ⇒ (id → allParts("Body").partId) :: Nil
    }
  }


  lazy val initialRobotParts = Seq(
    RobotPart(
      PartId(-1),
      name = PartName("Body"),
      serialNumber = SerialNumber.random,
      manufacturer = Manufacturer.BostonDynamics,
      weight = 9.9 kg
    ),
    RobotPart(
      PartId(-1),
      name = PartName("Arm"),
      serialNumber = SerialNumber.random,
      manufacturer = Manufacturer.Hitachi,
      weight = 2.2 kg
    ),
    RobotPart(
      PartId(-1),
      name = PartName("Leg"),
      serialNumber = SerialNumber.random,
      manufacturer = Manufacturer.Honda,
      weight = 3.0 kg
    ),
    RobotPart(
      PartId(-1),
      name = PartName("Head"),
      serialNumber = SerialNumber.random,
      manufacturer = Manufacturer.Honda,
      weight = 1.5 kg
    )
  )


}



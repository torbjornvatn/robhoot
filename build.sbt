import dependencies._
import sbt.Keys._
import sbt._

lazy val commonSettings = Seq(
    organization := "org.vatn",
    scalaVersion  := "2.11.6",
    version := "0.1")

lazy val robhoot = project.in(file(".")).settings(commonSettings:_*).settings(
  name := "robhoot-server",
  maintainer := "Torbjørn Vatn <torbjorn@vatn.org>",
  packageSummary := "Robhoot Server",
  mainClass in Compile := Some("Run"),
  libraryDependencies ++= deps
).enablePlugins(JavaAppPackaging)


val cli = project.in(file("cli")).settings(commonSettings:_*).settings(
  name := "robhoot-cli",
  maintainer := "Torbjørn Vatn <torbjorn@vatn.org>",
  packageSummary := "Robhoot CLI",
  mainClass in Compile := Some("CLI"),
  libraryDependencies ++= deps
).enablePlugins(JavaAppPackaging)
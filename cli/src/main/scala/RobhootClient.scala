
import argonaut.Argonaut._
import argonaut._
import com.ning.http.client.Request
import dispatch.Defaults._
import dispatch._

trait RobhootClient {
  def insertOrUpdateRobotPart(id: Option[Int], name: String, serialNumber: String, manufacturer: String, weight: String, compatibilities: Set[Int]): String
  def getRobotPart(partId: Int): String
  def getAllRobotParts: String
  def deleteRobotPart(partId: Int): String
  def getXCompatibleParts(numberOfParts: Int): String
  def printDatabaseSchema: String
}

class DispatchRobhootClient extends RobhootClient {

  val rawHost = url("http://localhost:5007")
  val host = rawHost / "robotParts"

  override def insertOrUpdateRobotPart(id: Option[Int], name: String, serialNumber: String, manufacturer: String, weight: String, compatibilities: Set[Int]): String = {
    val json = Json.obj(
      "part" := Json.obj(
        "name"             := name,
        "serialNumber"     := serialNumber,
        "manufacturer"     := manufacturer,
        "weight"           := weight.toDouble
      ),
      "compatibleParts" := Json.array(compatibilities.map(jNumber).toSeq:_*)
    )
    val req: (Request, OkFunctionHandler[String]) = id match {
      case Some(i)  ⇒ (host / i).PUT << json.spaces4 OK as.String
      case None     ⇒ host << json.spaces4 OK as.String
    }
    Http(req).apply()
  }

  override def deleteRobotPart(partId: Int): String = {
    val req = Http((host / partId).DELETE > as.String )
    req()
  }

  override def getXCompatibleParts(numberOfParts: Int): String = {
    val req: (Request, OkFunctionHandler[String]) = rawHost / "compatibleParts" <<? Map("numberOfParts" → s"$numberOfParts") OK as.String
    Http(req).apply()
  }

  override def printDatabaseSchema: String = {
    val req = Http(host / "schema" OK as.String )
    req()
  }

  override def getRobotPart(partId: Int): String = {
    val req = Http(host / partId OK as.String)
    req()
  }

  override def getAllRobotParts: String = {
    val req = Http(host OK as.String)
    req()
  }
}

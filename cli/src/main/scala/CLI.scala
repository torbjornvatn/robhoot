import sbt.complete.Parser

object CLI {

  def main(args: Array[String]): Unit = {
    val robhoot = new DispatchRobhootClient
    parseLine(args.mkString(" "), RobhootCommand.parser) match {
      case Some(cmd) => cmd.run(robhoot)
      case None => runCli(robhoot)
    }
  }

  def runCli(robhoot: RobhootClient): Unit = {
    def readCommand(): Option[RobhootCommand] = {
      readLine(RobhootCommand.parser)
    }
    def loop(): Unit = readCommand() match {
      case None       => WAT.run(); loop()
      case Some(Exit) => () // Exit
      case Some(cmd) =>
        cmd.run(robhoot)
        loop()
    }
    println(
      s"""
         |Welcome to the Robhoot cli
         |Type help to see what your options are
         |
       """.stripMargin)
    loop()
  }

  def readCommand(): Option[RobhootCommand] = {
    readLine(RobhootCommand.parser)
  }

  /** Uses SBT complete library to read user input with a given auto-completing parser. */
  private def readLine[U](parser: Parser[U], prompt: String = "robhoot> ", mask: Option[Char] = None): Option[U] = {
    val reader = new sbt.FullReader(None, parser)
    reader.readLine(prompt, mask) flatMap { line =>
      parseLine(line, parser)
    }
  }

  // Parses a line of input
  private def parseLine[U](line: String, parser: Parser[U]): Option[U] = {
    val parsed = Parser.parse(line, parser)
    parsed match {
      case Right(value) => Some(value)
      case Left(e) => None
    }
  }
}

sealed trait RobhootCommand {
  def run(client: RobhootClient): Unit
}

object RobhootCommand {
  import sbt.complete._
  import DefaultParsers._

  val parser: Parser[RobhootCommand] = {
    val exit = token("exit" ^^^ Exit)
    val help = token("help" ^^^ Help)
    val listParts = token("list" ^^^ ListParts)
    val schema = token("schema" ^^^ Schema)
    val compatibleParts = {
      ((token("compatibleParts") ~ Space) ~> token(IntBasic, "[numberOfParts]")) map {
        nr => CompatibleParts(nr)
      }
    }
    val deletePart = {
      ((token("delete") ~ Space) ~> token(IntBasic, "[id]")) map {
        id => DeletePart(id)
      }
    }
    val part = {
      ((token("part") ~ Space) ~> token(IntBasic, "[id]")) map {
        id => GetPart(id)
      }
    }
    val partParts =
      (token(StringBasic, "[name]") <~ Space) ~
      (token(StringBasic, "[serialNumber]") <~ Space) ~
      (token(StringBasic, "[manufacturer]") <~ Space) ~
      (token(StringBasic, "[weight]") <~ Space) ~
      token(repsep(IntBasic, ","), "[compatibilities]")

    val addPart = {
      ((token("add") ~ Space) ~>
        partParts) map {
        case ((((name, serialNumber), manufacturer), weight), comps) ⇒
          AddPart(name, serialNumber, manufacturer, weight, comps.toSet)
      }
    }

    val updatePart = {
      ((token("update") ~ Space) ~>
        (token(IntBasic, "[id]") <~ Space) ~
        partParts) map {
        case (id, ((((name, serialNumber), manufacturer), weight), comps)) ⇒
          UpdatePart(id, name, serialNumber, manufacturer, weight, comps.toSet)
      }
    }

    exit | help | schema | listParts | part | addPart | updatePart | deletePart | compatibleParts
  }
}

object Exit extends RobhootCommand {
  override def run(client: RobhootClient): Unit = ()
}

object WAT  {
  def run(): Unit = {
    System.out.println(s"-- WAT! --")
  }
}

object Help extends RobhootCommand  {
  def run(client: RobhootClient): Unit = {
    System.out.println(
      s"""
         | Cli client that communicates with the eminent Robhoot service
         |
         | It supports this commands (with tab completion):
         |
         | list                                                                       - Lists all robot parts
         | part [id]                                                                  - Displays robot part with [id]
         | add [name] [serialNumber] [manufacturer] [weight] [comp. part, *]          - Adds a new robot part
         | update [id] [name] [serialNumber] [manufacturer] [weight] [comp. part, *]  - Updates a robot part
         | delete [id]                                                                - Updates a robot part
         | compatibleParts [numberOfParts]                                            - List X compatible parts
         | schema                                                                     - Prints the database schema
         | exit                                                                       - Exits the Cli
         | help                                                                       - You're reading it
       """.stripMargin)
  }
}

object ListParts extends RobhootCommand {
  override def run(client: RobhootClient): Unit = {
    System.out.println(s"-- ALL THE PARTS! --")
    System.out.println(s"${client.getAllRobotParts}")
  }
}

object Schema extends RobhootCommand {
  override def run(client: RobhootClient): Unit = {
    System.out.println(s"-- THE DATABASE SCHEMA! --")
    System.out.println(s"${client.printDatabaseSchema}")
  }
}

case class GetPart(partId: Int) extends RobhootCommand {
  override def run(client: RobhootClient): Unit = {
    System.out.println(s"-- A GREAT ROBOT PART! --")
    System.out.println(s"${client.getRobotPart(partId)}")
  }
}

case class DeletePart(partId: Int) extends RobhootCommand {
  override def run(client: RobhootClient): Unit = {
    System.out.println(s"-- DELETING ROBOT PART! --")
    System.out.println(s"${client.deleteRobotPart(partId)}")
  }
}

case class AddPart(name: String, serialNumber: String, manufacturer: String, weight: String, compatibilities: Set[Int]) extends RobhootCommand {
  override def run(client: RobhootClient): Unit = {
    System.out.println(s"-- ADDING ROBOT PART! --")
    System.out.println(s"${client.insertOrUpdateRobotPart(None, name, serialNumber, manufacturer, weight, compatibilities)}")
  }
}

case class UpdatePart(id: Int, name: String, serialNumber: String, manufacturer: String, weight: String, compatibilities: Set[Int]) extends RobhootCommand {
  override def run(client: RobhootClient): Unit = {
    System.out.println(s"-- UPDATING ROBOT PART! --")
    System.out.println(s"${client.insertOrUpdateRobotPart(Some(id), name, serialNumber, manufacturer, weight, compatibilities)}")
  }
}

case class CompatibleParts(numberOfParts: Int) extends RobhootCommand {
  override def run(client: RobhootClient): Unit = {
    System.out.println(s"-- GETTING $numberOfParts COMPATIBLE ROBOT PARTS! --")
    System.out.println(s"${client.getXCompatibleParts(numberOfParts)}")
  }
}
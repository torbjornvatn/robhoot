import sbt._

object dependencies {

  lazy val deps = Seq(
    unfiltered.directives,
    unfiltered.filter,
    unfiltered.jetty,
    argonaut,
    config,
    completion,
    dispatch
  ) ++ slick

  object unfiltered {
    val version       = "0.9.0-beta1"
    val directives    = "net.databinder" %% "unfiltered-directives"   % version
    val filter        = "net.databinder" %% "unfiltered-filter"       % version
    val filterAsync   = "net.databinder" %% "unfiltered-filter-async" % version
    val jetty         = "net.databinder" %% "unfiltered-jetty"        % version
  }

  val slick = Seq(
    "com.typesafe.slick" %% "slick" % "3.0.0",
    "org.slf4j" % "slf4j-nop" % "1.6.4",
    "com.h2database" % "h2" % "1.4.187"
  )

  val config = "com.typesafe" % "config" % "1.3.0"

  val argonaut = "io.argonaut" %% "argonaut" % "6.1"

  val completion =  "org.scala-sbt" % "completion" % "0.13.8"

  val dispatch = "net.databinder.dispatch" %% "dispatch-core" % "0.11.2"
}
